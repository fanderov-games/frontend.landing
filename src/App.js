import React from 'react';
import Router from './components/Router';
import { Provider } from 'react-redux';
import { Dimmer, Loader } from 'semantic-ui-react';

import reducer from './redux/reducer';
import {loadState, saveState} from './redux/persistence';
import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';

class App extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			store: null,
		};
	}
	componentWillMount() {
		loadState().then((state) => {
			var store = createStore(reducer, state, applyMiddleware(logger));
			store.subscribe(() => {
				saveState(store.getState());
			});

			this.unsubscribe = store.subscribe(() =>  null);
			this.setState({
				store,
			});
		});
	}
	
	componentWillUnmount() {
		this.unsubscribe();
	}
	
	render() {
		return (
			<React.Fragment>
				{this.state.store ? (
					<Provider store={this.state.store}>
						<Router />
					</Provider>
				) : (
					<Dimmer active page inverted={this.state.store ? !this.state.store.get('settings').dark : false}>
			        	<Loader />
			        </Dimmer>
				)}
			</React.Fragment>
		);
	}
}

export default App;
