import React from 'react';
import { connect } from 'react-redux';
import { HashRouter, Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import StatePage from './pages/StatePage';

const mapStateToProps = state => {
    return {
        dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
    };
};

class Router extends React.Component {
    render() {
        if (this.props.dark) document.body.className = 'bodyBackgroundDark';
        else document.body.className = '';
        return (
            <HashRouter>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/login' component={LoginPage} />
                    <Route exact path='/register' component={RegisterPage} />
                    <Route exact path='/StatePage' component={StatePage}/>
                </Switch>
            </HashRouter>
        );
    }
}

export default connect(mapStateToProps)(Router);