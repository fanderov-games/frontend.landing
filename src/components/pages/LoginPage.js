import React from 'react';
import { Button, Form, Header, Image, Message, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import './LoginPage.scss';

const mapStateToProps = state => {
    return {
        dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
    };
};

class LoginPage extends React.Component {
    render() {
        return (
            <div className='login-form'>
                <Header inverted={this.props.dark} as='h2' color='green' textAlign='center'>
                    <Image circular src='https://i.railrunner16.me/LnXThSu.png' /> Log into your account
                </Header>
                <Form inverted={this.props.dark} size='large'>
                    <Segment inverted={this.props.dark} stacked>
                        <Form.Input fluid icon='user' iconPosition='left' placeholder='E-mail address' />
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                        />
            
                        <Button primary fluid size='large'>Login</Button>
                    </Segment>
                </Form>
                <Message color={this.props.dark ? 'black' : null}>
                    New to us? <Link to='/signup'>Sign Up</Link>
                </Message>
            </div>
        );
    }
}

export default connect(mapStateToProps)(LoginPage);
