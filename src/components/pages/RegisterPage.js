import React from 'react';
import { Form, Segment, Button, Input, Select, Header, Icon, Divider } from 'semantic-ui-react';
import { connect } from 'react-redux';

import Page from '../common/Page';

const mapStateToProps = state => {
	return {
		dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
	};
};

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            charisma: 0,
            charismaError: false,
            connections: 0,
            connectionsError: false,
            communication: 0,
            communicationError: false,
            stamina: 0,
            staminaError: false,
        };
        
        this.charismaInput = React.createRef();
        this.connectionsInput = React.createRef();
        this.communicationInput = React.createRef();
        this.staminaInput = React.createRef();
        
        this.onNumberStatChange = this.onNumberStatChange.bind(this);
        this.states = [
            { key: 'al', value: 'al', text: 'Alabama' },
            { key: 'ak', value: 'ak', text: 'Alaska' },
            { key: 'az', value: 'az', text: 'Arizona' },
            { key: 'ar', value: 'ar', text: 'Arkansas' },
            { key: 'ca', value: 'ca', text: 'California' },
            { key: 'co', value: 'co', text: 'Colorado' },
            { key: 'ct', value: 'ct', text: 'Connecticut' },
            { key: 'de', value: 'de', text: 'Delaware' },
            { key: 'dc', value: 'dc', text: 'District Of Columbia' },
            { key: 'fl', value: 'fl', text: 'Florida' },
            { key: 'ga', value: 'ga', text: 'Georgia' },
            { key: 'hi', value: 'hi', text: 'Hawaii' },
            { key: 'id', value: 'id', text: 'Idaho' },
            { key: 'il', value: 'il', text: 'Illinois' },
            { key: 'in', value: 'in', text: 'Indiana' },
            { key: 'ia', value: 'ia', text: 'Iowa' },
            { key: 'ks', value: 'ks', text: 'Kansas' },
            { key: 'ky', value: 'ky', text: 'Kentucky' },
            { key: 'la', value: 'la', text: 'Louisiana' },
            { key: 'me', value: 'me', text: 'Maine' },
            { key: 'md', value: 'md', text: 'Maryland' },
            { key: 'ma', value: 'ma', text: 'Massachusetts' },
            { key: 'mi', value: 'mi', text: 'Michigan' },
            { key: 'mn', value: 'mn', text: 'Minnesota' },
            { key: 'ms', value: 'ms', text: 'Mississippi' },
            { key: 'mo', value: 'mo', text: 'Missouri' },
            { key: 'mt', value: 'mt', text: 'Montana' },
            { key: 'ne', value: 'ne', text: 'Nebraska' },
            { key: 'nv', value: 'nv', text: 'Nevada' },
            { key: 'nh', value: 'nh', text: 'New Hampshire' },
            { key: 'nj', value: 'nj', text: 'New Jersey' },
            { key: 'nm', value: 'nm', text: 'New Mexico' },
            { key: 'ny', value: 'ny', text: 'New York' },
            { key: 'nc', value: 'nc', text: 'North Carolina' },
            { key: 'nd', value: 'nd', text: 'North Dakota' },
            { key: 'oh', value: 'oh', text: 'Ohio' },
            { key: 'ok', value: 'ok', text: 'Oklahoma' },
            { key: 'or', value: 'or', text: 'Oregon' },
            { key: 'pa', value: 'pa', text: 'Pennsylvania' },
            { key: 'ri', value: 'ri', text: 'Rhode Island' },
            { key: 'sc', value: 'sc', text: 'South Carolina' },
            { key: 'sd', value: 'sd', text: 'South Dakota' },
            { key: 'tn', value: 'tn', text: 'Tennessee' },
            { key: 'tx', value: 'tx', text: 'Texas' },
            { key: 'ut', value: 'ut', text: 'Utah' },
            { key: 'vt', value: 'vt', text: 'Vermont' },
            { key: 'va', value: 'va', text: 'Virginia' },
            { key: 'wa', value: 'wa', text: 'Washington' },
            { key: 'wv', value: 'wv', text: 'West Virginia' },
            { key: 'wi', value: 'wi', text: 'Wisconsin' },
            { key: 'wy', value: 'wy', text: 'Wyoming' },
        ];
        
        this.parties = [
            { key: 'r', value: 'r', text: 'Republican' },
            { key: 'd', value: 'd', text: 'Democrat' },
            { key: 'i', value: 'i', text: 'Independent' },
            { key: 'g', value: 'g', text: 'Green' },
            { key: 'a', value: 'a', text: 'Alliance' },
            { key: 'l', value: 'l', text: 'Libertarian' },
        ];
        
        this.classes = [
            { key: 'p', value: 'p', text: 'Poor' },
            { key: 'm', value: 'm', text: 'Middle Class' },
            { key: 'w', value: 'w', text: 'Wealthy' },
        ];
        
        this.races = [
            { key: 'w', value: 'w', text: 'White / Caucasian' },
            { key: 'b', value: 'b', text: 'Black / African American' },
            { key: 'h', value: 'h', text: 'Hispanic / Latino' },
            { key: 'a', value: 'a', text: 'Asian' },
            { key: 'o', value: 'o', text: 'Other' },
        ];
    }
    
    onNumberStatChange(e) {
        this.setState({
            [e.target.name]: parseInt(e.target.value, 10),
        });
    }
    
    render() {
        return (
            <Page>
                <Header as='h2' icon textAlign='center' inverted={this.props.dark}>
                    <Icon name='user plus' circular />
                    <Header.Content>Sign Up</Header.Content>
                </Header>
                <Form inverted={this.props.dark} size='large'>
                    <Segment inverted={this.props.dark} stacked>
                        <Divider horizontal>
                            <Header inverted={this.props.dark} as='h4'>
                                <Icon name='user' />
                                User Details
                            </Header>
                        </Divider>
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>Username</label>
                                <Input fluid icon='user' iconPosition='left' placeholder='Username' />
                            </Form.Field>
                            <Form.Field>
                                <label>Politician Name</label>
                                <Input fluid icon='user circle' iconPosition='left' placeholder='Politician Name' />
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>E-Mail Address</label>
                                <Input fluid icon='at' iconPosition='left' placeholder='E-Mail Address' />
                            </Form.Field>
                            <Form.Field>
                                <label>Password</label>
                                <Input fluid icon='lock' iconPosition='left' placeholder='Password' type='password' />
                            </Form.Field>
                        </Form.Group>
                        <Divider horizontal>
                            <Header inverted={this.props.dark} as='h4'>
                                <Icon name='bar chart' />
                                Stats
                            </Header>
                        </Divider>
                        <Segment inverted={this.props.dark} basic>
                            <b>NOTE:</b> The four statistical regions (Charisma, Connections, Communication, Stamina) must add up to 4. Money is not changeable.
                        </Segment>
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>Charisma</label>
                                <Input
                                    fluid
                                    name='charisma'
                                    ref={this.charismaInput}
                                    type='number'
                                    value={this.state.charisma}
                                    error={this.state.charismaError}
                                    onChange={this.onNumberStatChange}
                                    min={0}
                                    max={4}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>Connections</label>
                                <Input
                                    fluid
                                    name='connections'
                                    ref={this.connectionsInput}
                                    type='number'
                                    value={this.state.connections}
                                    error={this.state.connectionsError}
                                    onChange={this.onNumberStatChange}
                                    min={0}
                                    max={4}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>Communication</label>
                                <Input
                                    fluid
                                    name='communication'
                                    ref={this.communicationInput}
                                    type='number'
                                    value={this.state.communication}
                                    error={this.state.communicationError}
                                    onChange={this.onNumberStatChange}
                                    min={0}
                                    max={4}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>Stamina</label>
                                <Input
                                    fluid
                                    name='stamina'
                                    ref={this.staminaInput}
                                    type='number'
                                    value={this.state.stamina}
                                    error={this.state.staminaError}
                                    onChange={this.onNumberStatChange}
                                    min={0}
                                    max={4}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Divider horizontal>
                            <Header inverted={this.props.dark} as='h4'>
                                <Icon name='map marker alternate' />
                                Origins
                            </Header>
                        </Divider>
                        <Form.Group widths='equal'>
                            <Form.Field control={Select} label='State' options={this.states} placeholder='State' />
                            <Form.Field control={Select} label='Party' options={this.parties} placeholder='Party' />
                            <Form.Field control={Select} label='Class' options={this.classes} placeholder='Class' />
                            <Form.Field control={Select} label='Race' options={this.races} placeholder='Race' />
                        </Form.Group>
            
                        <Button primary fluid size='large'>Sign Up</Button>
                    </Segment>
                </Form>
            </Page>
        );
    }
}

export default connect(mapStateToProps)(RegisterPage);