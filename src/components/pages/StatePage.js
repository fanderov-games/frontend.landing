import  React from 'react';
import { connect } from 'react-redux';

import './StatePage.scss';

const mapStateToProps = state => {
    return {
        dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
    };
};

class StatePage extends React.Component {
    render() {
        return (
            <div className="ui container" style={{
                backgroundColor: this.props.dark ? '#000000' : '#ffffff', // this.props.dark is a boolean
            }}>
            <div id="wrapper" className="ui grid"> 
                
                <div id='summery' className="sixteen wide column">
                    <h1>State Name</h1>
                    <hr/>
                    <h4>State Tagline</h4>
                </div>

                <div id="Wrapper2" className="ui grid">
                <div id='description' className="ten wide column">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere risus in neque suscipit pharetra. Phasellus mollis eros a metus finibus dapibus. In at nibh bibendum sapien dapibus sagittis sit amet at ante. Aenean dignissim turpis ac ligula ullamcorper, a varius augue lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla et porttitor neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam sed nisl vitae velit maximus suscipit vitae quis ligula. Proin sit amet tortor et dui efficitur porttitor. Sed maximus sem nisl, at rhoncus ligula scelerisque quis. Suspendisse ultricies, urna at dictum lobortis, ligula felis sodales nibh, vel iaculis purus massa in mauris. Fusce ac velit congue, tincidunt enim quis, commodo est. Phasellus non auctor nisl.</p>
                </div>

                <div id="stats"  className="three wide column">
                    <ul>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>
                        <li>Stats</li>

                    </ul>
                </div>
            </div>
            </div>
            </div>
        );
    }
}

export default connect(mapStateToProps)(StatePage);