import React from 'react';

import GameCard from '../common/GameCard';
import Page from '../common/Page';

class Home extends React.Component {
    getDummyGames(num) {
        const games = [];
        for (let i = 0; i < num; i++) games.push({
            name: `Game ${i + 1}`,
            slug: `game-${i + 1}`,
            image: `https://via.placeholder.com/720x400.png?text=Game+${i + 1}`,
            description: 'Lorem ipsum dolor sit amet',
        });
        return games;
    }
    
    render() {
        return (
            <Page>
                <div className='ui inverted cards'>
                    {this.getDummyGames(12).map((game, index) => (<GameCard key={index} game={game} />))}
                </div>
            </Page>
        );
    }
}

export default Home;