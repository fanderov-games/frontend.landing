import React from 'react';
import { connect } from 'react-redux';
import { Icon, Grid, Header, Segment, Checkbox, List, Container } from 'semantic-ui-react';

const mapDispatchToProps = dispatch => {
	return {
		flipSetting(settingKey) {
			dispatch({
				type: 'flip_setting',
				settingKey: settingKey,
			});
		},
	};
};

const mapStateToProps = state => {
	return {
		dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
	};
};

class Footer extends React.Component {
	constructor(props) {
		super(props);
		
		this.changeTheme = this.changeTheme.bind(this);
	}
	
	changeTheme() {
		this.props.flipSetting('dark');
	}
	
	render() {
		return (
			<Segment inverted={this.props.dark} vertical style={{
				padding: '5em 0em',
				marginTop: '1.5em',
				borderTop: this.props.dark ? '' : '1px solid #ccc',
				background: this.props.dark ? '' : '#fff',
			}}>
				<Container>
					<Grid divided inverted={this.props.dark} stackable>
						<Grid.Row>
							<Grid.Column width={3}>
								<Header inverted={this.props.dark} as='h4' content='Links' />
								<List link inverted={this.props.dark}>
									<List.Item as='a' href='https://discord.gg/awAZUv6'><Icon name='discord' /> Our Discord</List.Item>
									<List.Item as='a' href='https://www.patreon.com/fanderov_games'><Icon name='patreon' /> Donate on Patreon</List.Item>
									<List.Item as='a' href='https://twitter.com/fanderov'><Icon name='twitter' /> Our Twitter</List.Item>
								</List>
							</Grid.Column>
							<Grid.Column width={3}>
								<Header inverted={this.props.dark} as='h4' content='Settings' />
								<List link inverted={this.props.dark}>
									<List.Item>Dark Mode <Checkbox style={{
										float: 'right',
									}} toggle defaultChecked={this.props.dark} onChange={this.changeTheme}/></List.Item>
								</List>
							</Grid.Column>
							<Grid.Column width={10}>
								<Header as='h4' inverted={this.props.dark}>Fanderov Games</Header>
								<p>The best games on the web</p>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Container>
			</Segment>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);