import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

const mapStateToProps = state => {
    return {
        dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
    };
};

class Nav extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			location: window.location.hash,
		};
		
		this.changeLocation = this.changeLocation.bind(this);
	}
	
	changeLocation() {
		this.setState({
			location: window.location.hash,
		});
	}
	
	componentDidMount() {
		window.addEventListener('hashchange', this.changeLocation, false);
	}

	componentWillUnmount() {
		window.removeEventListener('hashchange', this.changeLocation, false);
	}
	
	render() {
		return (
			<Menu inverted={this.props.dark} size='large' fixed='top'>
				<Menu.Item as={Link} onClick={this.changeLocation} active={this.state.location === '#/'} to='/'>Home</Menu.Item>
				<Menu.Menu position='right'>
					<Menu.Item as={Link} onClick={this.changeLocation} active={this.state.location === '#/register'} to='/register'>Sign up</Menu.Item>
					<Menu.Item as={Link} onClick={this.changeLocation} active={this.state.location === '#/login'} to='/login'>Log in</Menu.Item>
				</Menu.Menu>
			</Menu>
		);
	}
}

export default connect(mapStateToProps)(Nav);
