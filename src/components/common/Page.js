import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';

import Nav from './Nav';
import Footer from './Footer';

const mapStateToProps = state => {
    return {
        dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
    };
};

class Page extends React.Component {
	render() {
		return (
			<div className={this.props.dark ? 'bodyBackgroundDark' : ''}>
				<Nav />
				<Container style={{
					paddingTop: '5em',
				}}>
					{this.props.children}
				</Container>
				<Footer />
			</div>
		);
	}
}

export default connect(mapStateToProps)(Page);