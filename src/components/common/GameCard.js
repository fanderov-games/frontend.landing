import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Card, Image } from 'semantic-ui-react';
// import PropTypes from 'prop-types';

const mapStateToProps = state => {
    return {
        dark: state ? typeof state.get === 'function' ? state.get('settings').dark : true : true,
    };
};

class GameCard extends React.Component {
    render() {
        return (
            <Card className={this.props.dark ? 'inverted' : ''} style={{
                width: '32%',
            }}>
                <Image src={this.props.game.image} />
                <Card.Content>
                    <Card.Header>{this.props.game.name}</Card.Header>
                    <Card.Meta>Game</Card.Meta>
                    <Card.Description>{this.props.game.description}</Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <Button as={Link} to={`/game/${this.props.game.slug}`} fluid primary>Play!</Button>
                </Card.Content>
            </Card>
        );
    }
}

export default connect(mapStateToProps)(GameCard);