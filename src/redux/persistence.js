import Cookie from 'js-cookie';

export const loadState = async () => {
    const state = await Cookie.getJSON('settings');
    if (state === null) {
        return undefined;
    } else {
        return state;
    }
};

export const saveState = async (state) => {
    try {
        Cookie.set('settings', state.toJSON());
    } catch (e) {
        console.log(`Error setting cookie: ${e}`);
    }
};