import { Map } from 'immutable';

const reducer = (state, action) => {
    if (typeof state == 'undefined' || typeof state == 'boolean') {
        state = new Map({
            settings: {
                dark: true,
            }
        });
    }

    switch (action.type) {
        case 'flip_setting':
            console.log(Object.prototype.toString.call(state));
            var currentSetting = state.get('settings');
            var newSettings = Object.assign({}, currentSetting);
            newSettings[action.settingKey] = !currentSetting[action.settingKey];
            return state.set('settings', newSettings);
        default:
            return Map.isMap(state) ? state : Map(state);
    }
};

export default reducer;
